
document.getElementById("countButton").onclick = function() {

    //Var to keep all the typed text 
    let typedText = document.getElementById("textInput").value;

    typedText = typedText.toLowerCase();
    typedText = typedText.replace(/[^a-z'\s]+/g, ""); 


    //Will be keeping count of letters
    const letterCounts = {};

    //Getting each letter from each word
    for (let i = 0; i < typedText.length; i++) {
        currentLetter = typedText[i];

        if(letterCounts[currentLetter] === undefined) {
            letterCounts[currentLetter] = 1;
        } else {
            letterCounts[currentLetter]++;
        }
        
    }

    //Output each time a letter is used
    for (let letter in letterCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + letter + "\": " + letterCounts[letter] + ", ");
        span.appendChild(textContent);
        document.getElementById("lettersDiv").appendChild(span);
    }

    //Separate each word on the space
    const words = typedText.split(" ");

    //Will be keeping count of words
    const wordCounts = {};

    //Getting each word from typed text
    for (let i = 0; i < words.length; i++) {
        currentWord = words[i];
    
        if (wordCounts[currentWord] === undefined) {
            wordCounts[currentWord] = 1;
        } else {
            wordCounts[currentWord] ++;
        }
    }

    //Output each time a word is used
    for (let word in wordCounts) {
        const span = document.createElement("span");
        const textContent = document.createTextNode('"' + word + "\": " + wordCounts[word] + ", ");
        span.appendChild(textContent);
        document.getElementById("wordsDiv").appendChild(span);
    }
}